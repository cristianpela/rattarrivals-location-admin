import { Component, Input, Output, EventEmitter } from '@angular/core';
@Component({
    selector: 'content-editable',
    templateUrl: './dist/templates/content-editable.html'
})
export class ContentEditableComponent {
    @Input() data: MetaData;
    @Input() edit: boolean;
    @Output() valueEmitter: EventEmitter<MetaData> = new EventEmitter<MetaData>();


    emitValue(event: Event, newValue: number) {
        event.preventDefault();
        event.stopPropagation();
        this.toggleInput();
        this.data.value = newValue;
        this.valueEmitter.emit(this.data);
    }

    toggleInput() {
        this.edit = !this.edit;
    }

}

export interface MetaData {
    id: any;
    field?: string;
    value: any;
}