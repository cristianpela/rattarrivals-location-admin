import { Component, OnInit } from '@angular/core';
import {RemoteDBService} from './remotedb.service';
import {Location} from './location';
import {ContentEditableComponent, MetaData} from './contenteditable.component';
import {LocationFilter} from './location-filter'

@Component({
    selector: 'ratt-lox',
    templateUrl: './dist/templates/locations.html',
    providers: [RemoteDBService],
    directives: [ContentEditableComponent],
    pipes: [LocationFilter]
})
export class AppComponent implements OnInit {

    isLoading: boolean;
    toBeSaved: boolean;
    error: string;

    locations: Location[];

    constructor(private remoteDbService: RemoteDBService) {
    }

    ngOnInit() {
        this.isLoading = true;
        this.remoteDbService.getLocations().subscribe((locations: Location[]) => {
            this.locations = locations;
            this.isLoading = false;
        }, err => {
            this.error = err;
        });
    }

    valueListener(event) {
        let location: Location;
        for (let index = 0; index < this.locations.length; index++) {
            location = this.locations[index];
            if (location.id === event.id) {
                break;
            }
        }
        if (location[event.field] !== event.value) {
            location[event.field] = event.value;
            location.editable = true;
            this.toBeSaved = true;
        }
    }

    save(){
       for(let location of this.locations){
           if(location.editable)
            console.log("Location "  + location.toString());
       }
    }

    getData(location: Location, field: string): MetaData {
        return {
            id: location.id,
            field: field,
            value: location[field]
        };
    }

}
