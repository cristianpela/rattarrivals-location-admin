import { Pipe, PipeTransform } from '@angular/core';
import {Location} from './location'

@Pipe({ name: 'locationFilter', pure: false })
export class LocationFilter implements PipeTransform {
    transform(source: Location[], filter: string): Location[] {
       return filter ? source.filter((location:Location) => {
           return location.name.indexOf(filter) !== -1;
       }): source;
    }
}