import {Location} from './location';
import {Injectable} from '@angular/core';
import { Http, Response, RequestOptions, Headers} from '@angular/http';
import {Observable} from 'rxjs';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';

@Injectable()
export class RemoteDBService {

     private BASE_URL: string = "https://api.mlab.com/api/1/databases/rattarrivals/collections/locations?apiKey=AmZ1n8J7zFa8Tqrv4REqLp-318aps9vQ";
    
    private httpHeaders = new RequestOptions({
        headers: new Headers({ 
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': 'https://api.mlab.com'
         })
    });

    constructor(private http: Http) { }

    getLocations(): Observable<Location[]> {
        return this.http.get(this.BASE_URL).map((res: Response) => {
            let locations:Location[] = res.json().map(data =>{
                return {
                    id: data.station_id,
                    name: data.name,
                    latitude: data.latitude,
                    longitude: data.longitude,
                    editable: false
                }
            });
            return locations;
        });
    }

    private extractData(res: Response) {
        let body = res.json();
        return body || {};
    }

    private handleError(error: any) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Promise.reject(errMsg);
    }
}