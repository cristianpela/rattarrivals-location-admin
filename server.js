"use strict";
// <reference path="./typings/index.d.ts" />
var express = require("express");
var app = express();
app.use(express.static(__dirname));
app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});
app.listen(process.env.PORT || 3000, function () {
    console.log('Server has started...');
});
//# sourceMappingURL=index.js.map