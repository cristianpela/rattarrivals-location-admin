var gulp = require('gulp');
var jade = require('gulp-jade');
var jadeFilesPath = './dev/jade/**/*.jade';

gulp.task('jade', function (done) {
    return gulp.src(jadeFilesPath)
        .pipe(jade({ 
            pretty: true,
            doctype: 'html'
        }))
        .pipe(gulp.dest('dist/templates'));
});

gulp.task('watch', function () {
    gulp.watch([jadeFilesPath], ['jade']);
});

gulp.task('default', ['jade', 'watch']);
