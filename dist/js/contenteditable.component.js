"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var ContentEditableComponent = (function () {
    function ContentEditableComponent() {
        this.valueEmitter = new core_1.EventEmitter();
    }
    ContentEditableComponent.prototype.emitValue = function (event, newValue) {
        event.preventDefault();
        event.stopPropagation();
        this.toggleInput();
        this.data.value = newValue;
        this.valueEmitter.emit(this.data);
    };
    ContentEditableComponent.prototype.toggleInput = function () {
        this.edit = !this.edit;
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], ContentEditableComponent.prototype, "data", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], ContentEditableComponent.prototype, "edit", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', core_1.EventEmitter)
    ], ContentEditableComponent.prototype, "valueEmitter", void 0);
    ContentEditableComponent = __decorate([
        core_1.Component({
            selector: 'content-editable',
            templateUrl: './dist/templates/content-editable.html'
        }), 
        __metadata('design:paramtypes', [])
    ], ContentEditableComponent);
    return ContentEditableComponent;
}());
exports.ContentEditableComponent = ContentEditableComponent;
//# sourceMappingURL=contenteditable.component.js.map