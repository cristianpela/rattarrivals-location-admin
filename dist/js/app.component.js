"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var remotedb_service_1 = require('./remotedb.service');
var contenteditable_component_1 = require('./contenteditable.component');
var location_filter_1 = require('./location-filter');
var AppComponent = (function () {
    function AppComponent(remoteDbService) {
        this.remoteDbService = remoteDbService;
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.isLoading = true;
        this.remoteDbService.getLocations().subscribe(function (locations) {
            _this.locations = locations;
            _this.isLoading = false;
        }, function (err) {
            _this.error = err;
        });
    };
    AppComponent.prototype.valueListener = function (event) {
        var location;
        for (var index = 0; index < this.locations.length; index++) {
            location = this.locations[index];
            if (location.id === event.id) {
                break;
            }
        }
        if (location[event.field] !== event.value) {
            location[event.field] = event.value;
            location.editable = true;
            this.toBeSaved = true;
        }
    };
    AppComponent.prototype.save = function () {
        for (var _i = 0, _a = this.locations; _i < _a.length; _i++) {
            var location_1 = _a[_i];
            if (location_1.editable)
                console.log("Location " + location_1.toString());
        }
    };
    AppComponent.prototype.getData = function (location, field) {
        return {
            id: location.id,
            field: field,
            value: location[field]
        };
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'ratt-lox',
            templateUrl: './dist/templates/locations.html',
            providers: [remotedb_service_1.RemoteDBService],
            directives: [contenteditable_component_1.ContentEditableComponent],
            pipes: [location_filter_1.LocationFilter]
        }), 
        __metadata('design:paramtypes', [remotedb_service_1.RemoteDBService])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map