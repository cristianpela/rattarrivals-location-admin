"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
require('rxjs/add/operator/toPromise');
require('rxjs/add/operator/map');
var RemoteDBService = (function () {
    function RemoteDBService(http) {
        this.http = http;
        this.BASE_URL = "https://api.mlab.com/api/1/databases/rattarrivals/collections/locations?apiKey=AmZ1n8J7zFa8Tqrv4REqLp-318aps9vQ";
        this.httpHeaders = new http_1.RequestOptions({
            headers: new http_1.Headers({
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': 'https://api.mlab.com'
            })
        });
    }
    RemoteDBService.prototype.getLocations = function () {
        return this.http.get(this.BASE_URL).map(function (res) {
            var locations = res.json().map(function (data) {
                return {
                    id: data.station_id,
                    name: data.name,
                    latitude: data.latitude,
                    longitude: data.longitude,
                    editable: false
                };
            });
            return locations;
        });
    };
    RemoteDBService.prototype.extractData = function (res) {
        var body = res.json();
        return body || {};
    };
    RemoteDBService.prototype.handleError = function (error) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        var errMsg = (error.message) ? error.message :
            error.status ? error.status + " - " + error.statusText : 'Server error';
        console.error(errMsg); // log to console instead
        return Promise.reject(errMsg);
    };
    RemoteDBService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], RemoteDBService);
    return RemoteDBService;
}());
exports.RemoteDBService = RemoteDBService;
//# sourceMappingURL=remotedb.service.js.map